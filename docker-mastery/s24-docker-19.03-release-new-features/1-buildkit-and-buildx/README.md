
## docker buildx
```bash
docker buildx create --driver docker-container --name dckbuilder
```
```bash
docker buildx build --platform linux/amd64,linux/arm64,linux/arm/v7 -t asinduvg/releaseparty:multiarc .
```
```bash
docker buildx use dckbuilder
```
```bash
docker buildx build --platform linux/amd64,linux/arm64,linux/arm/v7 -t asinduvg/releaseparty:multiarc . --push
```
```bash
docker buildx imagetools inspect asinduvg/releaseparty:multiarc
```
